#ifndef SCREENCOPY_IMAGEARRAY_H
#define SCREENCOPY_IMAGEARRAY_H
#include <cstring>

template<typename T>
class ImageArray {
public:
    ImageArray()
        : rows(0), cols(0), data(nullptr) {

    }
    ImageArray(size_t rows, size_t cols)
            : rows(rows), cols(cols) {
        data = new T*[rows];
        data[0] = new T[rows*cols];
        for (int i = 1; i < rows; i++)
            data[i] = data[i - 1] + cols;
    }

    ImageArray(const ImageArray& other) {
        rows = other.rows;
        cols = other.cols;
        data = new T*[rows];
        data[0] = new T[rows*cols];
        for (int i = 1; i < rows; i++) {
            data[i] = data[i - 1] + cols;
        }

        std::memcpy(data[0], other.data[0], sizeof(T) * rows*cols);
    }

    ImageArray(ImageArray&& other)
            : rows(other.rows), cols(other.cols), data(other.data) {
        other.data = nullptr;
    }

    ImageArray& operator=(const ImageArray& other) {
        rows = other.rows;
        cols = other.cols;
        T** tmp_data = new T*[rows];
        tmp_data[0] = new T[rows*cols];
        for (int i = 1; i < rows; i++) {
            tmp_data[i] = tmp_data[i - 1] + cols;
        }
        std::memcpy(tmp_data[0], other.data[0], sizeof(T) *rows*cols);
        if (data) {
            delete[] data[0];
            delete[] data;
        }
        data = tmp_data;
    }

    ImageArray& operator=(ImageArray&& other) {
        if (this!=&other) {
            if (data) {
                delete[] data[0];
                delete[] data;
            }
            rows = other.rows;
            cols = other.cols;
            data = other.data;
            other.data = nullptr;
        }
    }

    ~ImageArray() {
        if (data) {
            delete[] data[0];
            delete[] data;
        }
    }

    T** Get() {
        return data;
    }

    T* operator[](int i) {
        return data[i];
    }

    void print() {
        for (int y = 0; y < rows; y++) {
            for (int x = 0; x < cols; x++) {
                printf("%d\t", data[y][x]);
            }
            printf("\n");
        }
    }

    size_t GetWidth() {
        return cols;
    }

    size_t GetHeight() {
        return rows;
    }

private:
    size_t rows;
    size_t cols;
    T** data;
};

#endif //SCREENCOPY_IMAGEARRAY_H
