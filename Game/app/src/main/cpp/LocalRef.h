#ifndef BASE_LOCAL_REF_H
#define BASE_LOCAL_REF_H

#include <cstddef>
#include <jni.h>

/**
 * Automatically deletes a JNI local reference on destruction.
 */
template <typename T>
class CLocalRef
{
public:

	CLocalRef(JNIEnv* pEnv, T pObject)
	: m_pEnv(pEnv)
	, m_pObject(pObject)
	{}

	CLocalRef(CLocalRef&& localRef)
	: m_pEnv(localRef.m_pEnv)
	, m_pObject(localRef.m_pObject)
	{
		localRef.m_pEnv = NULL;
		localRef.m_pObject = NULL;
	}

	~CLocalRef()
	{
		Reset();
	}

	void Reset(T pObject = NULL)
	{
		if (m_pObject == pObject)
		{
			return;
		}

		if (m_pObject != NULL)
		{
			m_pEnv->DeleteLocalRef(m_pObject);
		}

		m_pObject = pObject;
	}

	T Get() const { return m_pObject; }
	operator T() const { return m_pObject; }
	operator bool() const { return m_pObject != NULL; }

private:
	JNIEnv* m_pEnv;
	T m_pObject;

	CLocalRef(const CLocalRef&) = delete;
	CLocalRef& operator=(const CLocalRef&) = delete;
};


#endif // BASE_LOCAL_REF_H
