#ifndef SCREENCOPY_OCRUTILS_H
#define SCREENCOPY_OCRUTILS_H

#include <jni.h>
#include <android/log.h>
#include <iostream>
jobject createBitmap(JNIEnv* pEnv, int width, int height);
jstring STLStringToString(JNIEnv* pEnv, const std::string& sString);
std::string StringToSTLString(JNIEnv* pEnv, jstring stringObject);
#endif //SCREENCOPY_OCRUTILS_H
