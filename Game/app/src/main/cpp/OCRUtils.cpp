#include "OCRUtils.h"
#include <string>
#include <LocalRef.h>
#include <android/bitmap.h>
#include "common/CommonJNI.h"

#ifdef __BIG_ENDIAN
#define SK_A32_SHIFT 0
  #define SK_R32_SHIFT 8
  #define SK_G32_SHIFT 16
  #define SK_B32_SHIFT 24
#else
#define SK_A32_SHIFT 24
#define SK_R32_SHIFT 16
#define SK_G32_SHIFT 8
#define SK_B32_SHIFT 0
#endif /* __BIG_ENDIAN */

#define LOG_TAG "OCRUtils(native)"
#define LOGV(...) __android_log_print(ANDROID_LOG_VERBOSE, LOG_TAG, __VA_ARGS__)
#define LOGD(...) __android_log_print(ANDROID_LOG_DEBUG, LOG_TAG, __VA_ARGS__)
#define LOGI(...) __android_log_print(ANDROID_LOG_INFO, LOG_TAG, __VA_ARGS__)
#define LOGW(...) __android_log_print(ANDROID_LOG_WARN, LOG_TAG, __VA_ARGS__)
#define LOGE(...) __android_log_print(ANDROID_LOG_ERROR, LOG_TAG, __VA_ARGS__)
#define LOG_ASSERT(_cond, ...) if (!_cond) __android_log_assert("conditional", LOG_TAG, __VA_ARGS__)

std::string StringToSTLString(JNIEnv* pEnv, jstring stringObject)
{
    std::string sResult;

    if (stringObject)
    {
        const char* pszValue = pEnv->GetStringUTFChars(stringObject, NULL);
        sResult = pszValue;
        pEnv->ReleaseStringUTFChars(stringObject, pszValue);
    }

    return sResult;
}

jstring STLStringToString(JNIEnv* pEnv, const std::string& sString)
{
    CLocalRef<jclass> stringClass(pEnv, pEnv->FindClass("java/lang/String"));
    jmethodID id = pEnv->GetMethodID(stringClass, "<init>", "(" "[B" "Ljava/lang/String;" ")V");

    //create byte array with string
    size_t arraySize = sString.length();
    jbyteArray byteArray = pEnv->NewByteArray(arraySize);
    pEnv->SetByteArrayRegion(byteArray, 0, arraySize, (const jbyte*)sString.c_str());
    CLocalRef<jstring> encodingString(pEnv, pEnv->NewStringUTF("UTF-8"));
    jstring pObject = (jstring)pEnv->NewObject(stringClass.Get(), id, byteArray, encodingString.Get());

    pEnv->DeleteLocalRef(byteArray);

    return pObject;
}
