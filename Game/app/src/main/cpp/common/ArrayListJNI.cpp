#include "ArrayListJNI.h"
#include "DefinitionsJNI.h"

jclass    CArrayListJNI::CCache::m_pArrayListClass      = NULL;
jmethodID CArrayListJNI::CCache::m_arrayListConstructor = NULL;

CArrayListJNI::CArrayListJNI(JNIEnv* pEnv)
	: CListJNI(pEnv)
{
	m_pJavaObject.Reset(pEnv->NewObject(CCache::GetClass(),
    	CCache::GetConstructor()));
}

////////////////////////////////////////////////////////////////////////////////

void CArrayListJNI::CCache::InitClass(JNIEnv* pEnv)
{
	CLocalRef<jclass> arrayListClass(pEnv, pEnv->FindClass(JAVA_ARRAY_LIST_CLASS));
//    CS_ASSERT(arrayListClass, "CArrayListJNI no java " JAVA_ARRAY_LIST_CLASS " class found");
    m_pArrayListClass = (jclass)pEnv->NewGlobalRef(arrayListClass);

    m_arrayListConstructor = pEnv->GetMethodID(m_pArrayListClass,
        JAVA_CONSTRUCTOR, JAVA_DEFAULT_CONSTRUCTOR_SIG);
//    CS_ASSERT(m_pArrayListClass, "CArrayListJNI no default constructor "
//        "found in " JAVA_ARRAY_LIST_CLASS);
}

////////////////////////////////////////////////////////////////////////////////

jclass CArrayListJNI::CCache::GetClass()
{
	return m_pArrayListClass;
}

////////////////////////////////////////////////////////////////////////////////

jmethodID CArrayListJNI::CCache::GetConstructor()
{
	return m_arrayListConstructor;
}
