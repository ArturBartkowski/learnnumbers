#include "common/prediction/prediction.h"
#include <iomanip>
#include "THog.h"

using namespace std;

#define HOGW 64
#define HOGH 128


double dabs(double x)
{
	if (x>0) return x;
	return -x;
}

string to_stringstream(double** data, int type, int len)
{
	stringstream ss;
	ss << type << " ";
	int index = 0;
	for (int i = 0; i < len; i++)
	{
		for (int j = 0; j < 36; j++)
		{
			index++;
			if (!data[i][j]) continue;
			ss << index << ":" << data[i][j] << " ";
		}
	}
	ss << "-1" << endl;
	return ss.str();
}

string Add_HOG(unsigned char **input_img, int cols, int rows, int num)
{

	THog H1;
	H1.Initialize(cols,rows);
	H1.ComputeNew(input_img);
    int HBLOKSIZE = (HOGW/8-1)*(HOGH/8-1);
	string input = to_stringstream(H1.HBlock, num, HBLOKSIZE);

    return input;
}

string predict(unsigned char **input_img, int cols, int rows, int num)
{
    try {
        string result = Add_HOG(input_img, cols, rows, num);
        return result;
    }
    catch (exception ex)
    {
        return "-1";
    }


}
