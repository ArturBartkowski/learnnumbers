#include <math.h>

#include "THog.h"


THog::THog()
{
	HCel = HBlock = NULL;
	HCel_m = NULL;
	Dx = Dy = NULL;
	W = H = 0;
	HBLOKSIZE = 0;
}

void THog::Reset()
{
	memset( HCel[0], 0, Wc*Hc*9 *sizeof(double));
	memset( HBlock[0], 0, HBLOKSIZE*4*9 *sizeof(double));
	memset( HCel_m, 0, Wc*Hc*sizeof(double));
}


THog::~THog()
{
	if (HCel != NULL) {delete [] HCel[0]; delete [] HCel;}
	if (HCel_m != NULL) delete [] HCel_m;
	if (HBlock != NULL) {delete [] HBlock[0]; delete [] HBlock;}
	if (Dx != NULL) { delete [] Dx[0]; delete [] Dx;}
	if (Dy != NULL) { delete [] Dy[0]; delete [] Dy;}
}

void THog::Initialize(int w, int h)
{
	W = w;			//H i W powinny byc wielokrotnosciami 8
	H = h;
	Hc = H/8;
	Wc = W/8;
	HBLOKSIZE = (Wc-1)*(Hc-1);

	HCel = new_double_image(Wc*Hc, 9);
	HCel_m = new double[Wc*Hc];
	HBlock = new_double_image(HBLOKSIZE, 4*9);
	Dx = new_int_image(H, W);
	Dy = new_int_image(H, W);
}


void THog::Compute_Gradients_XY(unsigned char **a, int** dx, int** dy, int sx, int sy, int w, int h, int stride)
{
	int x,y;
	for (y=sy; y<sy+h; y++){
		for (x=sx; x<sx+w; x++){
			dx[y][x] = a[y][x+1] - a[y][x-1];
			dy[y][x] = a[y+1][x] - a[y-1][x];
		}
	}
}

double THog::Compute_HCel(int** Dx, int** Dy, double* HCel, int sx, int sy, int w, int h)
{ //return magnitude of HCel
	int x,y,b;				//b-bin number
	int dx, dy;
	double m=0.0;	//t-tangens, m-magnitude,vector lenght

	for (y=sy; y<sy+h; y++){
		for (x=sx; x<sx+w; x++){

			dx = Dx[y][x];
			dy = Dy[y][x];

			if (dx == 0) { if(dy>0) HCel[4]+=dy; else HCel[4]-=dy;}
			else if (dy == 0) { if(dx>0) HCel[0]+=dx; else HCel[0]-=dx;}
			else{
				double t = (double)dy/dx;
				if( t >=0 ){		//I & IV quart
					if		(t <= 0.363970) { b = 0;}
					else if (t <= 0.839100) { b = 1;}
					else if (t <= 1.191754) { b = 2;}
					else if (t <= 2.747477) { b = 3;}
					else { b = 4;}
				}
				else{												//I & IV quart
					if		(t <= -5.671282) { b = 5;}				//100st
					else if (t <= -1.732051) { b = 6;}
					else if (t <= -0.839100) { b = 7;}
					else if (t <= -0.363970) { b = 8;}
					else { b = 4;}
				}

				HCel[b] += sqrt((double)(dx*dx + dy*dy));
			}
			//sprobowac z inna metryka np. wartosci bezwzgledne
		}//for x
	}//for y

	for (b=0; b<9; b++) {
		m+= HCel[b]*HCel[b];
	}

	return m;
}

void THog::Compute_HBlock(int i,int j, double** HCel, double* HCel_m, double* block)
{
	int b, n;

	n = i*Wc+j;
	double sum = HCel_m[n] + HCel_m[n+1] + HCel_m[n+Wc] + HCel_m[n+Wc+1];

	if (sum !=0.0){
		sum = sqrt(sum);
		//normalize
		for(b=0; b<9; b++){
			block[b] = HCel[n][b]/sum;
			block[b+9] = HCel[n+1][b]/sum;
			block[b+18] = HCel[n+Wc][b]/sum;
			block[b+27] = HCel[n+Wc+1][b]/sum;
		}
	}
}

void THog::ComputeNew(unsigned char **input_img)
{
	int x,y,i,j;


	Reset();
	a = input_img;
	Compute_Gradients_XY(a, Dx, Dy, 1, 1, W-2, H-2, W);


	for(y=0; y<Hc; y++){
		for(x=0; x<Wc; x++){
			HCel_m[y*Wc+x] = Compute_HCel(Dx, Dy, HCel[y*Wc+x], x*8, y*8, 8, 8);
		}
	}

	//blocks..
	unsigned int idx = 0;
	for(i=0; i<Hc-1; i++){			//blokow jest o 1 mniej niz komorek, z definicji...
		for(j=0; j<Wc-1; j++){
			Compute_HBlock(i,j,HCel,HCel_m, HBlock[idx++]);
		}
	}

}