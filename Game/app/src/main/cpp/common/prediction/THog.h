#if !defined(DEF_T_HOG)
#define DEF_T_HOG

#include "t_utils.h"


class THog
{
public:
	double **HCel;
	double *HCel_m;
	double **HBlock;
	int HBLOKSIZE;
	int **Dx;
	int **Dy;

	int W, H, Wc, Hc;		//Wc ilosc cels w wierszu, Hc - w kolumnie
	unsigned char **a;

	// constructors
	THog();
	//THog(unsigned char **input_img, int w, int h, int HBlockSize);

	// routines
	//void Initialize(int w, int h, int HBlockSize);
	void Initialize(int w, int h);
	void Compute_Gradients_XY(unsigned char **a, int** dx, int** dy, int sx, int sy, int w, int h, int stride);
	double Compute_HCel(int** Dx, int** Dy, double* HCel, int sx, int sy, int w, int h);
	void Compute_HBlock(int i,int j, double** HCel, double* HCel_m, double* block);
	void ComputeNew(unsigned char **input_img);
	void Reset();
	// destructors
	~THog();
};





#endif