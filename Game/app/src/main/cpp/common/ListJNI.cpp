#include "ListJNI.h"

jclass    CListJNI::CCache::m_pListClass        = NULL;
jmethodID CListJNI::CCache::m_addToListMethod   = NULL;
jmethodID CListJNI::CCache::m_getFromListMethod = NULL;
jmethodID CListJNI::CCache::m_sizeOfListMethod  = NULL;

CListJNI::CListJNI(JNIEnv* pEnv, jobject pObject)
	: m_pJavaObject(pEnv, pEnv->NewLocalRef(pObject))
{
	CheckAssignable(pEnv, pEnv->GetObjectClass(pObject), CCache::GetClass());
}

////////////////////////////////////////////////////////////////////////////////

CListJNI::CListJNI(JNIEnv* pEnv)
	: m_pJavaObject(pEnv, NULL)
{
}

////////////////////////////////////////////////////////////////////////////////

void CListJNI::CheckAssignable(JNIEnv* pEnv, jclass javaClass, jclass baseClass)
{
	jboolean bAssignable =
    	pEnv->IsAssignableFrom(javaClass, baseClass);
//    CS_ASSERT(bAssignable, "Supplied argument is not subclass of Java List");
}

////////////////////////////////////////////////////////////////////////////////

void CListJNI::Add(JNIEnv* pEnv, jobject pObject)
{
	pEnv->CallBooleanMethod(m_pJavaObject.Get(), CCache::GetAddToListMethod(), pObject);
}

////////////////////////////////////////////////////////////////////////////////

jobject CListJNI::Get(JNIEnv* pEnv, int i)
{
	return pEnv->CallObjectMethod(m_pJavaObject, CCache::GetGetFromListMethod(), i);
}

////////////////////////////////////////////////////////////////////////////////

size_t CListJNI::Size(JNIEnv* pEnv)
{
	return pEnv->CallIntMethod(m_pJavaObject, CCache::GetSizeOfListMethod());
}

////////////////////////////////////////////////////////////////////////////////

jobject CListJNI::GetJavaObject(JNIEnv* pEnv)
{
	return pEnv->NewLocalRef(m_pJavaObject);
}

////////////////////////////////////////////////////////////////////////////////

void CListJNI::CCache::InitClass(JNIEnv* pEnv)
{
	CLocalRef<jclass> listClass(pEnv, pEnv->FindClass(JAVA_LIST_INTERFACE));
//    CS_ASSERT(listClass, "CListJNI no java " JAVA_LIST_INTERFACE " class found");
    m_pListClass = (jclass)pEnv->NewGlobalRef(listClass);

    m_addToListMethod = pEnv->GetMethodID(m_pListClass, JAVA_LIST_ADD_METHOD,
        JAVA_LIST_ADD_METHOD_SIG);
//    CS_ASSERT(m_addToListMethod, "CListJNI no " JAVA_LIST_ADD_METHOD
//        JAVA_LIST_ADD_METHOD_SIG " method found in " JAVA_LIST_INTERFACE);

    m_getFromListMethod = pEnv->GetMethodID(m_pListClass, JAVA_LIST_GET_METHOD,
        JAVA_LIST_GET_METHOD_SIG);
//    CS_ASSERT(m_getFromListMethod, "CListJNI no " JAVA_LIST_GET_METHOD
//        JAVA_LIST_GET_METHOD_SIG " method found in " JAVA_LIST_INTERFACE);

    m_sizeOfListMethod = pEnv->GetMethodID(m_pListClass, JAVA_LIST_SIZE_METHOD,
        JAVA_LIST_SIZE_METHOD_SIG);
//    CS_ASSERT(m_sizeOfListMethod, "CListJNI no " JAVA_LIST_SIZE_METHOD
//        JAVA_LIST_SIZE_METHOD_SIG " method found in " JAVA_LIST_INTERFACE);
}

////////////////////////////////////////////////////////////////////////////////

jclass CListJNI::CCache::GetClass()
{
	return m_pListClass;
}

////////////////////////////////////////////////////////////////////////////////

jmethodID CListJNI::CCache::GetAddToListMethod()
{
	return m_addToListMethod;
}

////////////////////////////////////////////////////////////////////////////////

jmethodID CListJNI::CCache::GetGetFromListMethod()
{
	return m_getFromListMethod;
}

////////////////////////////////////////////////////////////////////////////////

jmethodID CListJNI::CCache::GetSizeOfListMethod()
{
	return m_sizeOfListMethod;
}