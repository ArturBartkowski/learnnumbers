#ifndef COMMON_LIST_JNI_H
#define COMMON_LIST_JNI_H

#include <functional>
#include <jni.h>
#include <vector>
#include <LocalRef.h>
#include <CompilerDefinitions.h>

#define JAVA_LIST_INTERFACE       "java/util/List"
#define JAVA_LIST_ADD_METHOD      "add"
#define JAVA_LIST_ADD_METHOD_SIG  "(Ljava/lang/Object;)Z"
#define JAVA_LIST_GET_METHOD      "get"
#define JAVA_LIST_GET_METHOD_SIG  "(I)Ljava/lang/Object;"
#define JAVA_LIST_SIZE_METHOD     "size"
#define JAVA_LIST_SIZE_METHOD_SIG "()I"

class CListJNI
{
private:
	CS_DISALLOW_COPY_AND_ASSIGN(CListJNI);

public:
	class CCache
	{
	private:
		CS_DISALLOW_COPY_AND_ASSIGN(CCache);
		CCache();

	public:
		static void InitClass(JNIEnv* pEnv);
		static jclass GetClass();
		static jmethodID GetAddToListMethod();
		static jmethodID GetGetFromListMethod();
		static jmethodID GetSizeOfListMethod();

	private:
		static jclass m_pListClass;
		static jmethodID m_addToListMethod;
		static jmethodID m_getFromListMethod;
		static jmethodID m_sizeOfListMethod;
	};

public:
	CListJNI(JNIEnv* pEnv, jobject pObject);
	virtual ~CListJNI() {}

	virtual void Add(JNIEnv* pEnv, jobject pObject);
	virtual jobject Get(JNIEnv* pEnv, int i);
	virtual size_t Size(JNIEnv* pEnv);
	jobject GetJavaObject(JNIEnv* pEnv);

	/**
	* Converts Java List to STL vector.
	* @param  pEnv Java Environment
	* @param  convertFunction function that takes jobject as argument and returns associated native object
	* @return vector of native objects
	*/
	template <typename T>
	std::vector<T> ToVector(JNIEnv* pEnv, std::function<T(jobject)> convertFunction);

	/**
	 * Adds vector of native objects to Java List.
	 * @param pEnv Java Environment
	 * @param nativeObjects STL vector of native objects
	 * @param convertFunction function that takes native object as argument and returns associated jobject
	 */
	template <typename T>
	void AddVector(JNIEnv* pEnv, const std::vector<T>& nativeObjects, std::function<jobject(const T&)> convertFunction);

protected:
	CListJNI(JNIEnv* pEnv);

private:
	void CheckAssignable(JNIEnv* pEnv, jclass javaClass, jclass baseClass);

protected:
	CLocalRef<jobject> m_pJavaObject;
};

template <typename T>
std::vector<T> CListJNI::ToVector(JNIEnv* pEnv, std::function<T(jobject)> convertFunction)
{
	size_t size = Size(pEnv);
	std::vector<T> nativeObjects;

	for (size_t i = 0; i < size; ++i)
	{
		CLocalRef<jobject> pJavaObject(pEnv, Get(pEnv, i));
		nativeObjects.push_back(
			convertFunction(pJavaObject.Get()));
	}
	return nativeObjects;
}

template <typename T>
void CListJNI::AddVector(JNIEnv* pEnv, const std::vector<T>& nativeObjects, std::function<jobject(const T&)> convertFunction)
{
	for (typename std::vector<T>::const_iterator it = nativeObjects.begin(); it != nativeObjects.end(); ++it)
	{
		CLocalRef<jobject> pJavaObject(pEnv, convertFunction(*it));
		Add(pEnv, pJavaObject.Get());
	}
}


#endif // COMMON_LIST_JNI_H
