#ifndef COMMON_DEFINITIONS_JNI_H
#define COMMON_DEFINITIONS_JNI_H

#define JAVA_STRING_SIG              "Ljava/lang/String;"
#define JAVA_CONSTRUCTOR             "<init>"
#define JAVA_DEFAULT_CONSTRUCTOR_SIG "()V"

#endif // COMMON_DEFINITIONS_JNI_H
