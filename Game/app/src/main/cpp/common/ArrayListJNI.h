#ifndef COMMON_ARRAY_LIST_JNI_H
#define COMMON_ARRAY_LIST_JNI_H

#include "ListJNI.h"

#define JAVA_ARRAY_LIST_CLASS "java/util/ArrayList"

class CArrayListJNI : public CListJNI
{
private:
	CS_DISALLOW_COPY_AND_ASSIGN(CArrayListJNI);

public:
	class CCache
	{
	private:
		CS_DISALLOW_COPY_AND_ASSIGN(CCache);
		CCache();

	public:
		static void InitClass(JNIEnv* pEnv);
		static jclass GetClass();
		static jmethodID GetConstructor();

	private:
		static jclass m_pArrayListClass;
	    static jmethodID m_arrayListConstructor;
	};

public:
	CArrayListJNI(JNIEnv* pEnv);
	virtual ~CArrayListJNI() {}
};


#endif // COMMON_ARRAY_LIST_JNI_H
