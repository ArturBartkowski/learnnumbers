#ifndef BASE_COMPILER_DEFINITIONS_H
#define BASE_COMPILER_DEFINITIONS_H

/*
 * This file defines macros for compiler-specific features or extensions.
 */

/**
 * Explicitly marks a member function as deleted, preventing generation of that
 * function.
 */
#if defined(__clang__) || defined(_MSC_VER)
#define CS_DELETED_FUNCTION = delete
#else
#define CS_DELETED_FUNCTION
#endif

#define CS_DISALLOW_ASSIGN(type)\
	void operator=(const type&) CS_DELETED_FUNCTION

#define CS_DISALLOW_COPY_AND_ASSIGN(type)\
	type(const type&) CS_DELETED_FUNCTION;\
	CS_DISALLOW_ASSIGN(type)

#define CS_DISALLOW_COPY_MOVE_AND_ASSIGN(type) \
	type(type&&) CS_DELETED_FUNCTION; \
	type& operator=(type&&) CS_DELETED_FUNCTION; \
	CS_DISALLOW_COPY_AND_ASSIGN(type)

/**
 * Emits a warning if the result of a function or method is unused.
 *
 * This is useful in cases where not using the result is a bug. For example:
 *
 * @code{.cpp}
 * string Trim(string str) CS_WARN_UNUSED_RESULT;
 *
 * string str = " one ";
 * Trim(str);
 * // Proceed assuming the value of str has been trimmed.
 * @endcode
 *
 * This would result in a warning for code that erroneously assumed that Trim()
 * modified the string in-place.
 */
#if defined(__GNUC__)
#define CS_WARN_UNUSED_RESULT __attribute__((warn_unused_result))
#else
#define CS_WARN_UNUSED_RESULT
#endif

/*
 * Provides branch prediction hints to the compiler that may allow it to produce
 * more optimized code.
 *
 * CS_LIKELY surrounds a Boolean expression that is likely to be true.
 * CS_UNLIKELY surrounds a Boolean expression that is unlikely to be true.
 *
 * These should only be used for cases where the expression is very, very likely
 * or very, very unlikely.
 */
#if defined(__GNUC__)
#define CS_LIKELY(expression) __builtin_expect((bool)(expression), true)
#define CS_UNLIKELY(expression) __builtin_expect((bool)(expression), false)
#else
#define CS_LIKELY(expression) (expression)
#define CS_UNLIKELY(expression) (expression)
#endif

#endif // BASE_COMPILER_DEFINITIONS_H
