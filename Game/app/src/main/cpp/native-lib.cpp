#include <string>
#include <android/bitmap.h>
#include <cmath>
#include "ImageArray.h"
#include "common/prediction/prediction.h"
#include "common/CommonJNI.h"
#include "OCRUtils.h"

#define  LOG_TAG    "ScreenCopy"
#define  LOGD(...)  __android_log_print(ANDROID_LOG_DEBUG,LOG_TAG,__VA_ARGS__)
#define LOGE(...) __android_log_print(ANDROID_LOG_ERROR,LOG_TAG,__VA_ARGS__)

const int k = 13;

static inline uint32_t SetARGBInline(uint8_t a, uint8_t r, uint8_t g, uint8_t b)
{
    return (a << 24) | (r << 16) | (g << 8) | (b << 0);
}

/** return the alpha byte from a SkColor value */
#define GetA(color)      (((color) >> 24) & 0xFF)
/** return the red byte from a SkColor value */
#define GetR(color)      ((uint8_t)(((color) >> 16) & 0xFF))
/** return the green byte from a SkColor value */
#define GetG(color)      ((uint8_t)(((color) >>  8) & 0xFF))
/** return the blue byte from a SkColor value */
#define GetB(color)      ((uint8_t)(((color) >>  0) & 0xFF))

jobject createBitmap(JNIEnv* env, int width, int height)
{
    CLocalRef<jclass> bitmapCls(env, env->FindClass("android/graphics/Bitmap"));
    jmethodID createBitmapFunction = env->GetStaticMethodID(bitmapCls,
                                                            "createBitmap",
                                                            "(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;");
    CLocalRef<jstring> configName(env, env->NewStringUTF("ARGB_8888"));
    CLocalRef<jclass> bitmapConfigClass(env, env->FindClass("android/graphics/Bitmap$Config"));
    jmethodID valueOfBitmapConfigFunction = env->GetStaticMethodID(
            bitmapConfigClass, "valueOf",
            "(Ljava/lang/String;)Landroid/graphics/Bitmap$Config;");
    CLocalRef<jobject> bitmapConfig(env, env->CallStaticObjectMethod(bitmapConfigClass,
                                                                     valueOfBitmapConfigFunction, configName.Get()));
    jobject newBitmap = env->CallStaticObjectMethod(bitmapCls,
                                                    createBitmapFunction, width,
                                                    height, bitmapConfig.Get());
    return newBitmap;
}

jint JNI_OnLoad(JavaVM* vm, void* reserved)
{
    JNIEnv* env;
    if (vm->GetEnv(reinterpret_cast<void**>(&env), JNI_VERSION_1_6) != JNI_OK) {
        return -1;
    }

    CListJNI::CCache::InitClass(env);
    CArrayListJNI::CCache::InitClass(env);

    return JNI_VERSION_1_6;
}

ImageArray<uint8_t> convertBitmapToGrayscale(JNIEnv* env, jobject bitmap, AndroidBitmapInfo& bitmapInfo) {
    ImageArray<uint8_t> image(bitmapInfo.height, bitmapInfo.width);
    void* bitmapPixels;
    int ret;
    if ((ret = AndroidBitmap_lockPixels(env, bitmap, &bitmapPixels)) < 0)
    {
        LOGE("AndroidBitmap_lockPixels() failed ! error=%d", ret);
        return ImageArray<uint8_t>();
    }
    uint32_t* src = (uint32_t*) bitmapPixels;
    uint8_t** dst = image.Get();
    for (int i = 0; i < bitmapInfo.width * bitmapInfo.height; i++)
    {
        uint8_t red = GetR(src[i]);
        uint8_t green = GetG(src[i]);
        uint8_t blue = GetB(src[i]);
        dst[0][i] = static_cast<uint8_t>((0.299 * red) + (0.587 * green) + (0.114 * blue));
    }
    AndroidBitmap_unlockPixels(env, bitmap);
    return image;
}

void integralImageForBladley(unsigned char** g, uint64_t** I, int w, int h)
{
    uint64_t sum = 0;
    for (int y = 0; y < h; y++) {
        for (int x = 0; x < w; x++) {
            sum += g[y][x];
            if (y == 0) {
                I[y][x] = sum;
            }
            else {
                I[y][x] = I[y - 1][x] + sum;
            }
        }
        sum = 0;
    }
}

void integralImageForSavuola(unsigned char** g, uint64_t** I_squared, int w, int h)
{
    uint64_t sum = 0;
    for (int y = 0; y < h; y++) {
        for (int x = 0; x < w; x++) {
            sum += g[y][x] * g[y][x];
            if (y == 0) {
                I_squared[y][x] = sum;
            }
            else {
                I_squared[y][x] = I_squared[y - 1][x] + sum;
            }
        }
        sum = 0;
    }
}

ImageArray<uint8_t> sauvolaIntegral(unsigned char** data, size_t rows, size_t cols) {
    ImageArray<uint8_t> imageArray(rows, cols);
    uint8_t **b = imageArray.Get();

    ImageArray<uint64_t> integral_sum(rows, cols);
    uint64_t **I_sum = integral_sum.Get();

    ImageArray<uint64_t> integral_square(rows, cols);
    uint64_t **I_square = integral_square.Get();

    integralImageForBladley(data, I_sum, cols, rows);
    integralImageForSavuola(data, I_square, cols, rows);
    double t = 0;
    double m = 0;
    double pixels = (2 * k + 1) * (2 * k + 1);

    for (int y = 0; y < rows; y++) {
        for (int x = 0; x < cols; x++) {
            int tmpX = x;
            int tmpY = y;

            if (x - k < 0) tmpX = k;
            if (x + k > cols - 1) tmpX = cols - 1 - k;
            if (y - k < 0) tmpY = k;
            if (y + k > rows - 1) tmpY = rows - 1 - k;
            double sum = 0;
            double squaresum = 0;
            if (tmpY == k) {
                if (tmpX > k) {
                    sum += I_sum[tmpY + k][tmpX + k] - I_sum[tmpY + k][tmpX - k - 1];
                    squaresum += I_square[tmpY + k][tmpX + k] - I_square[tmpY + k][tmpX - k - 1];
                } else {
                    sum += I_sum[tmpY + k][tmpX + k];
                    squaresum += I_square[tmpY + k][tmpX + k];
                }
            } else {
                if (tmpX > k) {
                    sum += I_sum[tmpY + k][tmpX + k] + I_sum[tmpY - k - 1][tmpX - k - 1] -
                           I_sum[tmpY - k - 1][tmpX + k] - I_sum[tmpY + k][tmpX - k - 1];
                    squaresum +=
                            I_square[tmpY + k][tmpX + k] + I_square[tmpY - k - 1][tmpX - k - 1] -
                            I_square[tmpY - k - 1][tmpX + k] - I_square[tmpY + k][tmpX - k - 1];
                } else {
                    sum += I_sum[tmpY + k][tmpX + k] - I_sum[tmpY - k - 1][tmpX + k];
                    squaresum += I_square[tmpY + k][tmpX + k] - I_square[tmpY - k - 1][tmpX + k];
                }
            }
            double avg = (double) sum / pixels;
            double deviation = sqrt(squaresum / pixels - avg * avg);
            t = avg * (1 + 0.2 * (deviation / 128.0 - 1));
            b[y][x] = (uint8_t) ((data[y][x] > t) ? 255 : 0);
        }
    }
    return imageArray;
}

extern "C"
JNIEXPORT jstring JNICALL
Java_com_example_artur_game_HOGUtils_HOG(JNIEnv *env, jclass type, jobject bitmap, jint number) {

    AndroidBitmapInfo bitmapInfo;
    uint32_t* storedBitmapPixels = nullptr;
    int ret;
    if ((ret = AndroidBitmap_getInfo(env, bitmap, &bitmapInfo)) < 0)
    {
        LOGE("AndroidBitmap_getInfo() failed ! error=%d", ret);
        return NULL;
    }
    //std::string path = StringToSTLString(env, jpath);
    ImageArray<uint8_t> grayscale = convertBitmapToGrayscale(env, bitmap, bitmapInfo);
    std::string find = predict(grayscale.Get(), grayscale.GetWidth(), grayscale.GetHeight(), number);

    jstring output = STLStringToString(env, find);
    return output;
}
