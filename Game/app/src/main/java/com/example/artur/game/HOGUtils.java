package com.example.artur.game;

import android.graphics.Bitmap;

/**
 * Created by Karol_Laptop_Y700 on 17.06.2017.
 */

public class HOGUtils {
    static {
        System.loadLibrary("native-lib");
    }
    public static native String HOG(Bitmap bitmap, int number);
}
