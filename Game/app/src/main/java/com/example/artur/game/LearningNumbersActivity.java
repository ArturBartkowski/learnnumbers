package com.example.artur.game;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LearningNumbersActivity extends AppCompatActivity {

    @BindView(R.id.painterCanvas)
    Painter painter;
    @BindView(R.id.counter)
    TextView counter;
    @BindView(R.id.learningNumberLayout)
    ConstraintLayout learningNumberLayout;
    @BindView(R.id.ps)
    ImageButton ps;
    PrintScreen printScreen = new PrintScreen();
    StoryCountDown countDownTimer;
    private int str = 0;
    private int readNumber = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_learning_numbers);
        ButterKnife.bind(this);
        ps.setVisibility(View.GONE);
    }

    @Override
    protected void onDestroy() {
        if (countDownTimer != null) {
            countDownTimer.cancel();
        }
        countDownTimer = null;
        painter = null;
        super.onDestroy();
    }

    @OnClick(R.id.backToMenu)
    public void backToMenu(View control) {
        if (countDownTimer != null) {
            countDownTimer.cancel();
        }
        finish();
    }

    @OnClick(R.id.timer)
    public void start() {
        if (countDownTimer != null) {
            countDownTimer.cancel();
            clearCanvas();
        }
        ps.setVisibility(View.VISIBLE);
        changeBackground();
        startTimer();
    }

    public void startTimer() {
        countDownTimer = new StoryCountDown(20000, 1000);
        countDownTimer.start();
    }

    private int randomBackgroundDigits() {
        Random r = new Random();
        str = r.nextInt(10);
        return str;
    }

    private void changeBackground() {
        int randomDigit = randomBackgroundDigits();
        final String str = "a" + randomDigit;
        learningNumberLayout.setBackground(getResources().getDrawable(getResourceID(str, "drawable", getApplicationContext())));
    }

    private void changeBackgroundWrong(int str) {
        String currentNumber = "a" + str;
        learningNumberLayout.setBackground(getResources().getDrawable(getResourceID(currentNumber, "drawable", getApplicationContext())));
    }

    protected final static int getResourceID
            (final String resName, final String resType, final Context ctx) {
        final int ResourceID =
                ctx.getResources().getIdentifier(resName, resType,
                        ctx.getApplicationInfo().packageName);
        if (ResourceID == 0) {
            throw new IllegalArgumentException
                    (
                            "No resource string found with name " + resName
                    );
        } else {
            return ResourceID;
        }
    }

    private class StoryCountDown extends CountDownTimer {

        public StoryCountDown(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onTick(long millisUntilFinished) {
            counter.setText("" + millisUntilFinished / 1000);
        }

        @Override
        public void onFinish() {
            printScreen();
            checkAnswer(readNumber);
        }
    }

    private void printScreen() {
        View rootView = getWindow().getDecorView().findViewById(android.R.id.content);
        Bitmap screen = printScreen.getScreenShot(rootView, this);
        printScreen.store(screen, "screen.png", this);
        readNumber = printScreen.getHog(screen, str, this);
    }

    @OnClick(R.id.clear)
    public void clearCanvas() {
        painter.clearCanvas();
    }

    public void correctMassage() {
        Toast.makeText(getApplicationContext(), "Super, napisałeś poprawnie liczbę" + " " + str, Toast.LENGTH_LONG).show();
    }

    public void wrongMassage() {
        if (readNumber == -1) {
            Toast.makeText(getApplicationContext(), "Źle, postaraj się dokładniej napisać cyfrę" + " " + str, Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(getApplicationContext(), "Źle, napisałeś liczbę" + " " + readNumber, Toast.LENGTH_LONG).show();
        }
    }

    public void checkAnswer(int answer) {
        if (answer == str) {
            correctMassage();
            changeBackground();
            startTimer();
            clearCanvas();
        } else {
            wrongMassage();
            changeBackgroundWrong(str);
            startTimer();
            clearCanvas();
        }
    }

    @OnClick(R.id.ps)
    public void simplePS() {
        if (countDownTimer != null) {
            countDownTimer.cancel();
        }
        printScreen();
        checkAnswer(readNumber);
    }
}



