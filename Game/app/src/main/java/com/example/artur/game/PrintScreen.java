package com.example.artur.game;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Point;
import android.media.MediaScannerConnection;
import android.os.Environment;
import android.view.View;
import android.util.DisplayMetrics;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

/**
 * Created by Artur on 15.06.2017.
 */

public class PrintScreen {

    public Bitmap getScreenShot(View view, Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        View screenView = view.getRootView();
        screenView.setDrawingCacheEnabled(true);
        Bitmap bitmap = Bitmap.createBitmap(screenView.getDrawingCache(), 0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
        bitmap = Bitmap.createScaledBitmap(bitmap, 444, 270, false);
        bitmap = crop(bitmap);
        screenView.setDrawingCacheEnabled(false);
        return bitmap;
    }

    public int getHog(Bitmap map, int currentNumber, Context ctx) {
        String hog = HOGUtils.HOG(map, currentNumber);
        storeTxt(ctx,hog, "Hog_result.txt", false);
        final String dirPath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/Hog/";
        int result = runSVM(dirPath);
        return result;
    }

    public int runSVM(String path)
    {
        String[] input = new String[3];
        input[0] = path + "Hog_result.txt";
        input[1] = path + "all.model";
        input[2] = path + "out";
        int result = 0;
        try {
            String tmp = svm_predict.svmStart(input);
            result = (int) Double.parseDouble(tmp);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NumberFormatException e) {
            return -1;
        }
        return result;
    }

    public void storeTxt(Context ctx, String hog, String fileName, Boolean append) {

        final String dirPath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/Hog/";
        try {
            File dir = new File(dirPath);
            if (!dir.exists())
                dir.mkdirs();
            File file = new File(dirPath, fileName);
            FileOutputStream fOut = new FileOutputStream(file, append);
            OutputStreamWriter myOutWriter = new OutputStreamWriter(fOut);
            myOutWriter.append(hog);
            myOutWriter.close();
            fOut.flush();
            fOut.close();
            MediaScannerConnection.scanFile(ctx, new String[]{file.toString()}, null, null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    Bitmap crop(Bitmap bitmap) {
        Point top = new Point(), bottom = new Point(), left = new Point(), right = new Point();
        for (int rows = 0; rows < bitmap.getHeight(); rows++) {
            for (int cols = 0; cols < bitmap.getWidth(); cols++) {
                if (bitmap.getPixel(cols, rows) == Color.BLACK) {
                    top = new Point(cols, rows);
                    rows = bitmap.getHeight();
                    break;
                }
            }
        }
        for (int rows = bitmap.getHeight() - 1; rows >= 0; rows--) {
            for (int cols = 0; cols < bitmap.getWidth(); cols++) {
                if (bitmap.getPixel(cols, rows) == Color.BLACK) {
                    bottom = new Point(cols, rows);
                    rows = -1;
                    break;
                }
            }
        }

        for (int cols = 0; cols < bitmap.getWidth(); cols++) {
            for (int rows = 0; rows < bitmap.getHeight(); rows++) {
                if (bitmap.getPixel(cols, rows) == Color.BLACK) {
                    left = new Point(cols, rows);
                    cols = bitmap.getWidth();
                    break;
                }
            }
        }
        for (int cols = bitmap.getWidth() - 1; cols >= 0; cols--) {
            for (int rows = 0; rows < bitmap.getHeight(); rows++) {
                if (bitmap.getPixel(cols, rows) == Color.BLACK) {
                    right = new Point(cols, rows);
                    cols = -1;
                    break;
                }
            }
        }
        Bitmap whiteBlack = createBlackAndWhite(bitmap);
        Bitmap croppedBitmap = Bitmap.createBitmap(whiteBlack, left.x - 10, top.y - 10, (right.x - left.x) + 20, (bottom.y - top.y) + 20);
        Bitmap scaledBitmap = Bitmap.createScaledBitmap(croppedBitmap, 64, 128, false);
        return scaledBitmap;
    }

    public void store(Bitmap bm, String fileName, Context context) {
        final String dirPath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/Hog/";
        File dir = new File(dirPath);
        if (!dir.exists())
            dir.mkdirs();
        File file = new File(dirPath, fileName);
        try {
            FileOutputStream fOut = new FileOutputStream(file);
            bm.compress(Bitmap.CompressFormat.PNG, 100, fOut);
            fOut.flush();
            fOut.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static Bitmap createBlackAndWhite(Bitmap src) {
        int width = src.getWidth();
        int height = src.getHeight();
        Bitmap bmOut = Bitmap.createBitmap(width, height, src.getConfig());
        int A, R, G, B;
        int pixel;
        for (int x = 0; x < width; ++x) {
            for (int y = 0; y < height; ++y) {
                pixel = src.getPixel(x, y);
                A = Color.alpha(pixel);
                R = Color.red(pixel);
                G = Color.green(pixel);
                B = Color.blue(pixel);
                int gray = (int) (0.2989 * R + 0.5870 * G + 0.1140 * B);
                if (gray > 128)
                    gray = 255;
                else
                    gray = 0;
                bmOut.setPixel(x, y, Color.argb(A, gray, gray, gray));
            }
        }
        return bmOut;
    }
}