package com.example.artur.game;

import android.Manifest;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.RuntimePermissions;

@RuntimePermissions
public class GameActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.goToLessonNumbers)
    public void goToLessonNumbers() {
        Log.d(GameActivity.class.getSimpleName(), "Requested permissions");
        GameActivityPermissionsDispatcher.startLesionActivityWithCheck(this);
    }

    @NeedsPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
    public void startLesionActivity() {
        Intent intent = new Intent(GameActivity.this, LearningNumbersActivity.class);
        startActivity(intent);
    }

    /*@OnClick(R.id.goToShipGame)
    public void goToShipGame() {
        Intent intent = new Intent(GameActivity.this, ShipAndCabinActivity.class);
        startActivity(intent);
    }*/

    /*@OnClick(R.id.goToCabinGame)
    public void goToCabinGame() {
        Intent intent = new Intent(GameActivity.this, ShipAndCabinActivity.class);
        startActivity(intent);
    }*/

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        GameActivityPermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults);
    }
}
