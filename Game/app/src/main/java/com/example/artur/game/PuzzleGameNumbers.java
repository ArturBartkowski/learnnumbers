package com.example.artur.game;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PuzzleGameNumbers extends AppCompatActivity {

    @BindView(R.id.painterCanvas)
    Painter painter;
    @BindView(R.id.firstNumber)
    TextView firstNumberP;
    @BindView(R.id.secnondNumber)
    TextView secondNumberP;
    @BindView(R.id.digit)
    TextView digitP;
    @BindView(R.id.counterP)
    TextView counter;
    PrintScreen printScreen = new PrintScreen();
    int idx;

    private PuzzleGameNumbers.StoryCountDown countDownTimer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_puzzle_game_numbers);
        ButterKnife.bind(this);
        setText();
        startCounter();
    }

    @Override
    protected void onDestroy() {
        countDownTimer.cancel();
        countDownTimer = null;
        super.onDestroy();
    }

    @OnClick(R.id.backToMenu)
    public void goToMenu() {
        countDownTimer.cancel();
        countDownTimer = null;
        Intent intent = new Intent(PuzzleGameNumbers.this, GameActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.clear)
    public void clearCanvas() {
        painter.clearCanvas();
    }

    private int randomBackgroundDigits() {
        Random r = new Random();
        final int str = r.nextInt(9);
        return str;
    }

    private String randomMathOperators() {
        String[] mathOperators = {"+", "-", "*"};
        int idx = new Random().nextInt(mathOperators.length);
        String randomMO = (mathOperators[idx]);
        return randomMO;
    }

    private void setText() {
        int firstNumber = randomBackgroundDigits();
        int secondNumber = randomBackgroundDigits();
        String firstN = Integer.toString(firstNumber);
        String secondN = Integer.toString(secondNumber);
        String mathOperator = randomMathOperators();
        if (firstNumber < secondNumber) {
            firstNumberP.setText(secondN);
            secondNumberP.setText(firstN);
        } else {
            firstNumberP.setText(firstN);
            secondNumberP.setText(secondN);
        }
        digitP.setText(mathOperator);
    }

    private class StoryCountDown extends CountDownTimer {

        public StoryCountDown(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onTick(long millisUntilFinished) {
            counter.setText("" + millisUntilFinished / 1000);
        }

        @Override
        public void onFinish() {
            printScreen();
            setText();
            clearCanvas();
            startCounter();
        }
    }

    private void startCounter() {
        countDownTimer = new PuzzleGameNumbers.StoryCountDown(20000, 1000);
        countDownTimer.start();
    }

    private void printScreen() {
        View rootView = getWindow().getDecorView().findViewById(android.R.id.content);
        Bitmap screen = printScreen.getScreenShot(rootView, this);
        //String hog = printScreen.getHog(screen);
        //printScreen.storeTxt(this, hog, "Hog.txt");
    }
}
